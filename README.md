### Installation
```
yarn install
npx sequelize-cli db:migrate
npx sequelize-cli db:seed:all
node index.js
```
Domyślnie powinno uruchomić się na localhoscie, port 8085

### Initially encrypt password:
securityHelper.encodeRequestValue:
```
encodeRequestValue(rawPassword)
```

### Output for string "Password":
```
U2FsdGVkX1+sVlOyePhU/P73ZCuTBx8EoIBmzWyURhk=
```

### Authorization header
```
Authorization: {{ JWT TOKEN }}
```

### Wykorzystane technologie (backend)
- express js
- sequelize
- sqlite3
- node.js wersja 16.16